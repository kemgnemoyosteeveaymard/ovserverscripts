#echo ${OPENVIDU_KEYSTORE_PASSWORD}
#echo ${OPENVIDU_KEYSTORE_ALIAS}
#echo ${OPENVIDU_SECRET}
#echo ${OPENVIDU_PUBLICURL}

/usr/bin/java -jar \
	-Dserver.ssl.key-store=/home/${USER}/scripts/keystore.jks \
	-Dserver.ssl.key-store-password=${OPENVIDU_KEYSTORE_PASSWORD} \
	-Dserver.ssl.key-alias=${OPENVIDU_KEYSTORE_ALIAS} \
	-Dopenvidu.recording=true \
	-Dopenvidu.recording.path=/home/${USER}/openviduRecords \
	-Dopenvidu.secret=${OPENVIDU_SECRET} \
	-Dopenvidu.publicurl=${OPENVIDU_PUBLICURL} \
        /home/kemsty/scripts/openvidu-server-2.9.0.jar
