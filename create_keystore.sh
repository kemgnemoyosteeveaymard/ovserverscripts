openssl pkcs12 \
	-export \
	-name edoctar \
	-in /etc/letsencrypt/live/server.testedoctar.ml/cert.pem \
	-inkey /etc/letsencrypt/live/server.testedoctar.ml/privkey.pem \
	-out p12keystore.p12
keytool -importkeystore \
	 -srckeystore p12keystore.p12 \
	 -srcstoretype pkcs12 \
	 -deststoretype pkcs12 \
	 -alias edoctar \
	 -destkeystore keystore.jks
rm p12keystore.p12
